package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");
		
		List<Double> lados = new ArrayList<>();
		
		boolean deveAdicionarNovoLado = true;
		while(deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (lados.size() + 1));
			
			double tamanhoLado = Double.parseDouble(scanner.nextLine());
			
			if(tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			}else {
				lados.add(tamanhoLado);
			}
		}
		
		System.out.println("Lados cadastrados!");
		System.out.println("Agora vamos calcular a área...");
		
		if(lados.size() == 0) {
			System.out.println("Forma inválida!");
		}else if(lados.size() == 1) {
			System.out.println("Eu identifiquei um circulo!");
			System.out.println("A área do circulo é " + (Math.PI * Math.pow(lados.get(0), 2)));
		}else if(lados.size() == 2) {
			System.out.println("Eu identifiquei um quadrado/retangulo!");
			System.out.println("A área do quadrado/retangulo é " + (lados.get(0) * lados.get(0)));
		}else if(lados.size() == 3) {
			System.out.println("Eu identifiquei um triangulo!");
			double ladoA = lados.get(0);
			double ladoB = lados.get(1);
			double ladoC = lados.get(2);
			if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
				double s = (ladoA + ladoB + ladoC) / 2;       
				double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
				System.out.println("A área do triangulo é " + (area));	
			}else {
				System.out.println("Mas o triangulo informado era inválido :/");
			}
		}else {
			System.out.println("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");
		}
		
	}

}
